# Development Expert (Midrange – including but not limited to Frontend, Backend, Mobile based technologies)

Situation: Set the scene and give the necessary details of your example.   
Task: Describe what your responsibility was in that situation.   
Action: Explain exactly what steps you took to address it.   
Result: Share what outcomes your actions achieved.   

## Application Responses

### Using bullet points list your technical expertise including technical language, specialism (if applicable), years of experience using the language, employer/company where used and any notable projects/deliverables using the technology and any qualifications or formal technical training you have completed (max 400 words)

- Javascript/Typescript (Node, Angular, Svelte, Inversity)
  11 years experience
  Services Australia
    - START agile release train
    - JETT agile release train
    - Jobseeker programme
    - Recognise
    - Child Support System Replacement programme
Wisdom Advertising & Design

- Java (WCEM/JSF)
6 years experience
Services Australia
    - START agile release train
    - JETT agile release train
    - Jobseeker programme
    - Child Support System Replacement programme

- HTML/CSS
  11 years experience
  Services Australia
    - START agile release train
    - JETT agile release train
    - Jobseeker programme
    - Recognise
    - Child Support System Replacement programme
  Wisdom Advertising & Design

- GraphQL (Apollo)
  2 years experience
  Services Australia
  - Recognise

- Docker
  2 years experience
  Services Australia
  - Recognise

- PostgreSQL
  2 years experience
  Services Australia
  - Recognise

- SAP BSP
  1 year experience
  Services Australia
  - Child Support System Replacement

### Demonstrate an example of when you’ve provided leadership to a team/group of people (directly or indirectly) using your highly developed technical skills to deliver operational objectives and quality solutions. Outline the situation, your skills, and the outcome (max 250 words)

Child Support Online Services  
Child Support Online Services was in progress of being built at pace, which lead to a policy first, user experience second product. A external review into the product indicated the need for a user experience overhall.  
I put myself forward with the newly created divisional user experience team to update the user experience and to leveraged my knowledge and experience in implementing design systems and front-end applications.  
I approached the problem on two fronts. Firstly, the java application structures needed to be updated to leverage modularity and reusability. This change was communicated within the development team as a goal and the user interface was sliced into component based on foundations of application structure and researched design systems. Secondly, I worked with the UX team to leverage the design system used for Centrelink to provide a level of continuity but also adapt changes based on external design systems and existing research such as that provided by the Nielson Norman Group. Changes were user tested at the ATO design centre and appropriatly addopted, modified or discarded.  
The co-ordination of work between these teams and the re-enforced focus on user experience produced a product that was well regarded by end-users and was WCAG compliant.  

### Demonstrate an example where you’ve provided specialist technical advice incorporating knowledge, analysis, risk mitigation to drive continuous improvement or change. Describe the situation, your responsibilities and the outcome. (max 250 words)

SAARA test automation    
My team and I was handed the SAARA project where the current state for development and work required was unknown except that a large development component was done but had not been tested.   
Our goal was to plan out how to deliver the project in a short timeframe and then execute it within a 4 month period.     
I started by mapping out the system requirements against the developed product to provide information on gaps in the system as well as providing a base to design a test strategy from. This was followed by several joint analysis and design sessions with the team including assigned testers. Our preliminary test strategy and execution plan indicated that we would not be able to meet the required deadline. I had insisted that the outcome could be achieved by moving towards baseload regression using test automation. This was supported by the testers as they had some experience crafting automated test cases.  
As our focus was regression end-to-end testing, I architected and built a framework using the business-driven development product Cucumber with Cypress; an end-to-end test product, that leveraged the detirministic and component based nature of Centrelink Online Services and Process Direct.
Using the framework and regression testing strategy we were able to complete the product by the deadline. The test automation framework was able to increase testing performance by over 1500% and contributed towards the viability and implementation of test automation for the Centrelink Online Services and Process Direct products.     


### Provide an example where you have developed and managed key strategic relationships and successfully communicated the strategic or technical direction of your team, project or agency. (max 250 words)

DevOps Guild.  
The DevOps guild is looking on how to design it's role within the agency. The guild leadership team is made up of people from many areas in the agency and each person has their own agenda.   
My goal was to strategically work with the other representatives on how to agree on a position, while also communicating the needs for the developer community.   
I communicated that the development teams needed to be able to implement DevOps individually due to the needs of the product and enforcement at this level would inhibit the ability to timely deliver quality products. This problem is the sole target that DevOps strives to achieve.    
The guild agreed that its responsibility is to provide high level expectations on implementing DevOps concepts and be a point of engagement and provide a forum for projects to discuss how to implement DevOps.   
