# Resume

## Employment History

### APS6 Senior Developer - Services Australia
04/2018 - Current

#### Key Responsibilities
- Leading a Multi-disciplinary Team.
- Line management of a team of APS staff and Contractors.
- Responsible for key deliverables of time-sensitive projects.
- Championing and facilitating the progression of Agile and DevOps ways of working.
- Development of complex Angular, Node and WCEM Java web application and Openshift containerized architecture.
- Maintaining product quality through assisting staff with code reviews and other assurance processes.

#### Key Achievements
- Development of a business-driven development test automation framework for the Centrelink SAARA project. Enabled a regression suite of over 150 tests reducing project risks and decreasing test execution time by over 1500%.
- Developed and architected the Recognise web application. Recognise is a wellness product for the CPS division to promote and enable recognition of staff achievements via awards and person-to-person messaging. 

### EL1 A/g Technical Lead - Department of Human Services
09/2018 - 08/2018

#### Key Responsibilities
- Line management of a team of APS staff and Contractors.
- Responsible for technical designs for features delivered on the WPIT START and JETT agile release trains.
- Centrelink Online Services governance for Brisbane deliverables and changes.
- Lead developer within an Multi-disciplinary Team.

#### Key Achievements
- Successful delivery of features to production using an Agile methodology.

### APS5 Developer - Department of Human Services
12/2015 - 04/2018

#### Key Responsibilities
- Lead developer of the Child Support Online Services team using Kanban.
- WCAG accessibility developer and reviewer for Child Support.
- Development of code modularization in the Java web system.
- User experience and design system designer and developer for Child Support Online Services.
- Release support and production application monitoring.
- Mentoring of junior staff and apprentices.
- Participating in external user experience sessions.

#### Key Achievements
- Delivering of Child Support Online Services including the Unauthenticated application and Pluto assessment application to production with high positive customer feedback.
- Developed and released successfully the Ministerial Contact Form product in an extremely short time frame.
- Winner of the 2015 Innovation award for developing a first-contact service delivery solution using Google Glass. Project Jabberwocky leveraged the use of augmented reality to increase staff safety and training productivity with support for other features through a scalable platform.
